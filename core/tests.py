from django.test import TestCase,Client
from django.urls import reverse
from django.contrib.auth.models import User
from core.models import Timezone,State,Country
# Create your tests here.
class CompanyCreate(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = reverse('core:company-add')
        user = User.objects.create(username='dev')
        user.set_password('testtest')
        user.save()
        timezone_obj={
            "utc": "Pacific/Honolulu",
            "label": "Hawaiian Standard Time",
            "alias": "HST",
            "offset": -10,
            "is_dst": False,
            "description": "(UTC-10:00) Hawaii",
            "enabled": True
        }
        timezone = Timezone.objects.create(**timezone_obj)
        self.valid_payload = {
            'company-name':'Test Company 1',
            'company-phone':'123445678891123',
            'company-email':'test@gmail.com',
            'company-timezone':timezone.id,
            'company-status':'Active',
            'address-TOTAL_FORMS': '2',
            'address-INITIAL_FORMS': '0',
            'address-MAX_NUM_FORMS': '',
        }
        self.invalid_payload ={
            'company-phone':'123445678891123',
            'company-email':'test@gmail.com',
            'company-timezone':timezone.id,
            'company-status':'Active',
            'address-TOTAL_FORMS': '2',
            'address-INITIAL_FORMS': '0',
            'address-MAX_NUM_FORMS': '',
        }
        user_login =self.client.login(username="dev", password="testtest")
        self.assertTrue(user_login)

        country_obj={
            "code": "us",
            "name": "United States",
            "name_plain": "United States",
            "is_active": False,
            "dial_code": "1",
            "flag": "USA",
            "capital": "Washington - New York",
            "region": "North America",
            "region_des": None
        }
        country = Country.objects.create(**country_obj)
        self.state = State.objects.create(country=country,code="al",name="Alabama")


    def test_create_valid_company(self):
        response = self.client.post(self.url,self.valid_payload)
        self.assertEquals(response.status_code,302)

    def test_create_invalid_company(self):
        response = self.client.post(self.url,self.invalid_payload)
        self.assertFormError(response,'form','name','This field is required.')

    def test_create_valid_company_with_address(self):
        payload = self.valid_payload
        payload['address-0-type']='Billing'
        payload['address-0-line_1']='Test Line1'
        payload['address-0-line_2']='Test Line2'
        payload['address-0-city']='Test City'
        payload['address-0-state']=self.state.code
        payload['address-0-zip']='Test Zip'
        response = self.client.post(self.url,payload)
        self.assertEquals(response.status_code,302)

    def test_create_valid_company_with_invalid_address(self):
        payload = self.valid_payload
        payload['address-0-line_1']='Test Line1'
        payload['address-0-line_2']='Test Line2'
        payload['address-0-city']='Test City'
        payload['address-0-state']=self.state.code
        payload['address-0-zip']='Test Zip'
        response = self.client.post(self.url,payload)
        self.assertFormError(response,'form','type','This field is required.')




