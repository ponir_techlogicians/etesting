from django.contrib.auth import get_user_model
from rest_framework import serializers

from core.models import Address, Company, Country, State, Timezone

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserModel
        exclude = ('password', )

class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    country = serializers.StringRelatedField(many=False)

    class Meta:
        model = State
        fields = '__all__'


class AddressSerializer(serializers.ModelSerializer):
    state = StateSerializer()
    country = CountrySerializer()

    class Meta:
        model = Address
        fields = '__all__'

class AddressCrateSerializerForCompany(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'

class TimezoneSerializer(serializers.ModelSerializer):

    class Meta:
        model = Timezone
        fields = '__all__'

class CompanySerializer(serializers.ModelSerializer):
    addresses = AddressSerializer(many=True, read_only=True)
    timezone =  TimezoneSerializer()
    class Meta:
        model = Company
        fields = '__all__'

class CompanyCreateUpdateSerializer(serializers.ModelSerializer):
    addresses = AddressCrateSerializerForCompany(many=True)
    class Meta:
        model = Company
        fields = '__all__'

    def create(self, validated_data):
        addresses = validated_data.pop('addresses')
        company = Company.objects.create(**validated_data)
        for address in addresses:
            company.addresses.create(**address)
        return company

    def update(self, instance, validated_data):
        addresses = validated_data.pop('addresses')
        company = Company.objects.filter(pk=instance.id).update(**validated_data)
        companyObj=Company.objects.get(pk=instance.id)
        companyObj.addresses.clear()
        for address in addresses:
            companyObj.addresses.create(**address)
        return companyObj


