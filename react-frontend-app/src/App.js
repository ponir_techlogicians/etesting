import React from 'react';
import logo from './logo.svg';
import {Route,BrowserRouter} from 'react-router-dom';
import './App.css';
import Layout from './layout/layout';
import CompanyList from './pages/company/companylist';
import CompanyCreateEdit from './pages/company/companycreateedit';
import CompanyView from './pages/company/companyview';
import CompanyEdit from './pages/company/companyedit';
import Aux from './hoc/aux';

function App() {
  let routes = (
      <Aux>
          <Route path="/" exact component={CompanyList}></Route>
          <Route path="/company/list" exact component={CompanyList}></Route>
          <Route path="/company/add" exact component={CompanyCreateEdit}></Route>
          <Route path="/company/details/:id" exact component={CompanyView}></Route>
          <Route path="/company/edit/:id" exact component={CompanyCreateEdit}></Route>
      </Aux>
  )
  return (
    <BrowserRouter>
        <Layout>
            {routes}
        </Layout>
    </BrowserRouter>
  );
}

export default App;
