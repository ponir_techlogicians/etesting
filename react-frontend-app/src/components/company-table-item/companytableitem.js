import React from 'react';
import {Link} from 'react-router-dom';
const companytableitem = (props)=>{
    const link = "/company/details/"+props.id;
    const editLink="/company/edit/"+props.id;
    return(
        <tr key={props.id}>
            <td data-type="Status"><Link to={editLink} title="Edit" data-toggle="tooltip"><i className="os-icon os-icon-pencil-1"></i> </Link></td>
            <td data-type="Status">{props.status}</td>
            <td data-type="Name">
                <Link to={link}>{props.name}</Link>
            </td>
            <td data-type="Phone">{props.phone}</td>
        </tr>
    )
}

export default companytableitem;