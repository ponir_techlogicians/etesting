import React from 'react';
const layout = (props) => (
    <div className="all-wrapper with-side-panel solid-bg-all">
        <div className="layout-w">
            <div className="menu-mobile menu-activated-on-click color-scheme-dark">
                <div className="mm-logo-buttons-w">
                    <a className="mm-logo" href="index.html"><img src='img/logo.png'/><span>SavvyBiz Test</span></a>
                    <div className="mm-buttons">
                        <div className="mobile-menu-trigger">
                        </div>
                    </div>
                </div>
            </div>
            <div className="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
                <div className="logo-w">
                    <a className="logo" href="index.html">
                        <div className="logo-element"></div>
                        <div className="logo-label">
                            SavvyBiz Test
                        </div>
                    </a>
                </div>
                <div className="logged-user-w avatar-inline">
                    <div className="logged-user-i">
                        <div className="avatar-w">
                            <img src="/img/avatar1.jpg" alt="" />
                        </div>
                        <div className="logged-user-info-w">
                            <div className="logged-user-name">
                                    dev
                            </div>
                            <div className="logged-user-role">
                                    Adminstrator
                            </div>
                        </div>
                    </div>
                </div>

                <h1 className="menu-page-header">
                    Page Header
                </h1>
                <ul className="main-menu">
                    <li className="sub-header">
                        <span>Layouts</span>
                    </li>
                    <li className="selected sub-menu">
                        <a href="index.html">
                            <div className="icon-w">
                                <div className="os-icon os-icon-layout"></div>
                            </div>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li className=" sub-menu">
                        <a href="layouts_menu_top_image.html">
                            <div className="icon-w">
                                <div className="os-icon os-icon-layers"></div>
                            </div>
                            <span>Menu Styles</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div className="content-w">
                <div className="top-bar color-scheme-transparent d-none d-sm-block">
                    <div className="top-menu-controls">
                        <div className="logged-user-w">
                            <div className="logged-user-i">
                                <div className="avatar-w">
                                    <img alt="" src='/img/avatar1.jpg'/>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <ul className="breadcrumb">
                    <li className="breadcrumb-item">
                        <a href="index.html">Home</a>
                    </li>
                    <li className="breadcrumb-item">
                        <a href="">Company</a>
                    </li>
                </ul>
                <div className="content-panel-toggler">
                    <i className="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                </div>
                <div className="content-i">
                    {props.children}
                </div>
            </div>
        </div>
        <div className="display-type"></div>
    </div>
)

export default layout;