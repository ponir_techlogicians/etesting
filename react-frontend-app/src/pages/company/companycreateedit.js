import React ,{Component} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';

class CompanyCreateEdit extends Component{

    state = {
        form:{
			companyName:{
				value:'',
				validation:{
					required:true,
					minLength:5
				},
				valid:false,
                formvalidClass:['form-control'],
			},
			companyPhone:{
				value:'',
				validation:{
					required:true,
                    minLength:15
				},
				valid:false,
                formvalidClass:['form-control'],
			},
            companyFax:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyEmail:{
				value:'',
				validation:{
					required:true,
                    email:true
				},
				valid:false,
                formvalidClass:['form-control'],
			},
            companyWebsite:{
				value:'',
				validation:{
					required:false
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyFacebook:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyTwitter:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyLinkedin:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyInstagram:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyYoutube:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            companyTimezone:{
				value:'',
				validation:{
					required:true,
				},
				valid:false,
                formvalidClass:['form-control'],
			},
            companyStatus:{
				value:'',
				validation:{
					required:true,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressTypeOne:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressLineOneOne:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressLineTwoOne:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressCityOne:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressStateOne:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressZipOne:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressDefaultOne:{
				value:false,
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressTypeTwo:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressLineOneTwo:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressLineTwoTwo:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressCityTwo:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressStateTwo:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressZipTwo:{
				value:'',
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},
            addressDefaultTwo:{
				value:false,
				validation:{
					required:false,
				},
				valid:true,
                formvalidClass:['form-control'],
			},

		},
        addressTypeTwoShow:false,
        addressTypeOneId:null,
        addressTypeTwoId:null,
		isFormValid:false,
        timezones:[],
        states:[],
        companyId:null,
    }

    inputChangeHandler(input,event){

		const value = event.target.value;
		const updatedForm = { ...this.state.form }
		const updatedFormElement = { ...updatedForm[input]}
        console.log(input,value);
		if(input!=='file'){
            if(input=='addressDefaultOne'||input=='addressDefaultTwo'){
                updatedFormElement.value=event.target.checked;
			    updatedFormElement.valid=this.checkValidity(value,updatedFormElement.validation)
            }else{
                updatedFormElement.value=value;
			    updatedFormElement.valid=this.checkValidity(value,updatedFormElement.validation)
            }

		}else{
			console.log(event.target.files)
			updatedFormElement.value=event.target.files[0];
			updatedFormElement.valid=true;
		}


		updatedForm[input]=updatedFormElement;

		let isFormValid = true;
		for(let input in updatedForm){
			isFormValid = updatedForm[input].valid&&isFormValid;
		}
		this.setState({form:updatedForm,isFormValid:isFormValid})
	}

    checkValidity(value,rules){

		let isValid = true;

		if(rules.required){
			isValid = (value.trim()!=='')&&isValid;
		}
		if(rules.minLength){
			isValid = (value.length>=rules.minLength)&&isValid;
		}
		if(rules.email){
			isValid = this.validateEmail(value)&&isValid;
		}

		return isValid;

	}

    validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(email).toLowerCase());
	}

    submitHandler(event){
        event.preventDefault();
		const data = this.state.form;
        console.log(this.state.form);
        console.log(this.state.isFormValid);
        if(!this.state.isFormValid){
            const updatedForm = { ...this.state.form };
            for(let input in updatedForm){
                    const updatedFormElement = { ...updatedForm[input]};
                    let isValid = updatedForm[input].valid;
                    if(!isValid){
                        updatedFormElement.formvalidclassName=['form-control','is-invalid'];
                    }else{
                        updatedFormElement.formvalidclassName=['form-control','is-valid'];
                    }
                    updatedForm[input]=updatedFormElement;
            }
            this.setState({form:updatedForm});
        }else{
            let addresses=[];
            if(this.state.addressTypeTwoShow){

                if(this.state.companyId==null){
                    addresses.push( {
                       type:this.state.form.addressTypeOne.value,
                       line_1:this.state.form.addressLineOneOne.value,
                       line_2:this.state.form.addressLineOneTwo.value,
                       city:this.state.form.addressCityOne.value,
                       state:this.state.form.addressStateOne.value,
                       zip:this.state.form.addressZipOne.value,
                       default:this.state.form.addressDefaultOne.value,
                       updated_by:1,

                   });

                    addresses.push({
                           type:this.state.form.addressTypeTwo.value,
                           line_1:this.state.form.addressLineOneTwo.value,
                           line_2:this.state.form.addressLineTwoTwo.value,
                           city:this.state.form.addressCityTwo.value,
                           state:this.state.form.addressStateTwo.value,
                           zip:this.state.form.addressZipTwo.value,
                           default:this.state.form.addressDefaultTwo.value,
                           updated_by:1,
                       });
                }else{
                    addresses.push( {
                       id:this.state.addressTypeOneId,
                       type:this.state.form.addressTypeOne.value,
                       line_1:this.state.form.addressLineOneOne.value,
                       line_2:this.state.form.addressLineOneTwo.value,
                       city:this.state.form.addressCityOne.value,
                       state:this.state.form.addressStateOne.value,
                       zip:this.state.form.addressZipOne.value,
                       default:this.state.form.addressDefaultOne.value,
                       updated_by:1,

                   });

                    addresses.push({
                           id:this.state.addressTypeTwoId,
                           type:this.state.form.addressTypeTwo.value,
                           line_1:this.state.form.addressLineOneTwo.value,
                           line_2:this.state.form.addressLineTwoTwo.value,
                           city:this.state.form.addressCityTwo.value,
                           state:this.state.form.addressStateTwo.value,
                           zip:this.state.form.addressZipTwo.value,
                           default:this.state.form.addressDefaultTwo.value,
                           updated_by:1,
                       });
                    }
            }else{
                if(this.state.companyId==null){
                    addresses.push( {
                       type:this.state.form.addressTypeOne.value,
                       line_1:this.state.form.addressLineOneOne.value,
                       line_2:this.state.form.addressLineOneTwo.value,
                       city:this.state.form.addressCityOne.value,
                       state:this.state.form.addressStateOne.value,
                       zip:this.state.form.addressZipOne.value,
                       default:this.state.form.addressDefaultOne.value,
                       updated_by:1,

                   });
                }else{
                     addresses.push( {
                       id:this.state.addressTypeOneId,
                       type:this.state.form.addressTypeOne.value,
                       line_1:this.state.form.addressLineOneOne.value,
                       line_2:this.state.form.addressLineOneTwo.value,
                       city:this.state.form.addressCityOne.value,
                       state:this.state.form.addressStateOne.value,
                       zip:this.state.form.addressZipOne.value,
                       default:this.state.form.addressDefaultOne.value,
                       updated_by:1,

                   });
                }

            }
            const data = {
               name: this.state.form.companyName.value,
               phone: this.state.form.companyPhone.value,
               email:this.state.form.companyEmail.value,
               website:this.state.form.companyWebsite.value,
               facebook:this.state.form.companyFacebook.value,
               twitter:this.state.form.companyTwitter.value,
               linkedin:this.state.form.companyLinkedin.value,
               instagram:this.state.form.companyInstagram.value,
               youtube:this.state.form.companyYoutube.value,
               timezone:this.state.form.companyTimezone.value,
               status:this.state.form.companyStatus.value,
               updated_by:1,
               addresses:addresses
            };
            //let formData = new FormData();
            //formData.append('data',JSON.stringify(data));
    //        {
    //    headers: {
    //        'Content-Type': 'application/json',
    //    }
    //}
            if(this.state.companyId==null){
                 axios.post("http://127.0.0.1:8000/api/core/company/",data).then(
                    (res)=>{
                        console.log(res);
                        if(res.status==201){
                            this.props.history.push('/company/details/'+res.data.id);
                        }
                    }
                )
            }else{
                axios.put("http://127.0.0.1:8000/api/core/company/"+this.state.companyId+"/",data).then(
                    (res)=>{
                        console.log(res);
                        if(res.status==200){
                            this.props.history.push('/company/details/'+res.data.id);
                        }
                    }
                )
            }

        }

	}

    componentDidMount(){
        axios.get('http://127.0.0.1:8000/api/core/timezone/')
            .then((res)=>{
                this.setState({timezones:res.data.results});
            });

        axios.get('http://127.0.0.1:8000/api/core/state/')
            .then((res)=>{
                this.setState({states:res.data.results});
            });

        const companyId = this.props.match.params.id;
        this.setState({companyId:companyId})

        if(companyId!=null){
            axios.get('http://127.0.0.1:8000/api/core/company/'+companyId).then(
                (res)=>{
                    console.log(res);
                    this.populateData(res.data);
                }
            );
        }
    }

   populateData(data){
        const updatedForm = { ...this.state.form };
        for(let input in updatedForm){
                const updatedFormElement = { ...updatedForm[input]};
                updatedFormElement.valid=true;
                if(input=='companyName'){
                    updatedFormElement.value=data.name;
                }
                if(input=='companyPhone'){
                    updatedFormElement.value=data.phone;
                }
                if(input=='companyFax'){
                    updatedFormElement.value=data.fax;
                }
                if(input=='companyEmail'){
                    updatedFormElement.value=data.email;
                }
                if(input=='companyWebsite'){
                    updatedFormElement.value=data.website;
                }
                if(input=='companyFacebook'){
                    updatedFormElement.value=data.facebook;
                }
                if(input=='companyTwitter'){
                    updatedFormElement.value=data.twitter;
                }
                if(input=='companyLinkedin'){
                    updatedFormElement.value=data.linkedin;
                }
                if(input=='companyInstagram'){
                    updatedFormElement.value=data.instagram;
                }
                if(input=='companyYoutube'){
                    updatedFormElement.value=data.youtube;
                }
                if(input=='companyTimezone'){
                    updatedFormElement.value=data.timezone.id;
                }
                if(input=='companyStatus'){
                    updatedFormElement.value=data.status;
                }
                if(data.addresses.length>0){
                    this.setState({addressTypeOneId:data.addresses[0].id});
                    if(input=='addressTypeOne'){
                        updatedFormElement.value=data.addresses[0].type;
                    }
                    if(input=='addressLineOneOne'){
                        updatedFormElement.value=data.addresses[0].line_1;
                    }
                    if(input=='addressLineTwoOne'){
                        updatedFormElement.value=data.addresses[0].line_2;
                    }
                    if(input=='addressCityOne'){
                        updatedFormElement.value=data.addresses[0].city;
                    }
                    if(input=='addressStateOne'){
                        updatedFormElement.value=data.addresses[0].state.code;
                    }
                    if(input=='addressZipOne'){
                        updatedFormElement.value=data.addresses[0].zip;
                    }
                    if(input=='addressDefaultOne'){
                        updatedFormElement.value=data.addresses[0].default;
                    }

                    if(data.addresses.length>1){
                        this.setState({addressTypeTwoShow:true});
                        this.setState({addressTypeTwoId:data.addresses[1].id});
                        if(input=='addressTypeTwo'){
                        updatedFormElement.value=data.addresses[1].type;
                        }
                        if(input=='addressLineOneTwo'){
                            updatedFormElement.value=data.addresses[1].line_1;
                        }
                        if(input=='addressLineTwoTwo'){
                            updatedFormElement.value=data.addresses[1].line_2;
                        }
                        if(input=='addressCityTwo'){
                            updatedFormElement.value=data.addresses[1].city;
                        }
                        if(input=='addressStateTwo'){
                            updatedFormElement.value=data.addresses[1].state.code;
                        }
                        if(input=='addressZipTwo'){
                            updatedFormElement.value=data.addresses[1].zip;
                        }
                        if(input=='addressDefaultTwo'){
                            updatedFormElement.value=data.addresses[1].default;
                        }
                    }

                }
                updatedForm[input]=updatedFormElement;
        }
        this.setState({form:updatedForm,isFormValid:true});
   }
   hideSection(event){
       event.preventDefault();
       this.setState({addressTypeTwoShow:false});
   }

   showSection(event){
       event.preventDefault();
       this.setState({addressTypeTwoShow:true});
   }

    render(){
        let timezoneDropdown = this.state.timezones.map(item=>
            <option key={item.id} value={item.id}>{item.utc}</option>
        );

        let stateDropdown = this.state.states.map(item=>
            <option key={item.code} value={item.code}>{item.name}</option>
        );

        return (
            <div className="content-box">
                    <div className="element-wrapper">
                        <div className="element-box col">
                            <h5 className="form-header"><i className="fal fa-layer-plus"></i> {this.state.companyId?<span>Edit Company</span>:<span>Add Company</span>}
                                <small>
                                    <Link to="/company/list/" title="View Companies" data-toggle="tooltip" className="float-right"><i className="fal fa-list"></i> Companies</Link>
                                </small>
                            </h5>
                            <div className="form-desc"></div>
                            <div className="row justify-content-md-center">
                                <div className="col-12 col-sm-11">

                                    <div className="content-box">
                                        <form>

                                                <div className="row">
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-name">Name</label><input type="text" name="company-name" value={this.state.form.companyName.value} onChange={(event)=>this.inputChangeHandler('companyName',event)}   className={this.state.form.companyName.formvalidClass.join(' ')} placeholder="Name" title="" required="" id="id_company-name"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-phone">Phone</label><input type="text" name="company-phone" value={this.state.form.companyPhone.value} onChange={(event)=>this.inputChangeHandler('companyPhone',event)} className={this.state.form.companyPhone.formvalidClass.join(' ')} placeholder="Phone" title="" required="" id="id_company-phone"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-fax">Fax</label><input type="text" name="company-fax" value={this.state.form.companyFax.value} onChange={(event)=>this.inputChangeHandler('companyFax',event)}  className={this.state.form.companyFax.formvalidClass.join(' ')} placeholder="Fax" title="" id="id_company-fax"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-email">Email</label><input type="email" name="company-email" value={this.state.form.companyEmail.value} onChange={(event)=>this.inputChangeHandler('companyEmail',event)} className={this.state.form.companyFax.formvalidClass.join(' ')} placeholder="Email" title="" required="" id="id_company-email"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-website">Website</label><input type="url" name="company-website" value={this.state.form.companyWebsite.value} onChange={(event)=>this.inputChangeHandler('companyWebsite',event)}  className={this.state.form.companyFax.formvalidClass.join(' ')} placeholder="Website" title="" id="id_company-website"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-facebook">Facebook link</label><input type="url" name="company-facebook" value={this.state.form.companyFacebook.value} onChange={(event)=>this.inputChangeHandler('companyFacebook',event)}   className={this.state.form.companyFax.formvalidClass.join(' ')} placeholder="Facebook link" title="" id="id_company-facebook"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-twitter">Twitter link</label><input type="url" name="company-twitter" value={this.state.form.companyTwitter.value} onChange={(event)=>this.inputChangeHandler('companyTwitter',event)} className={this.state.form.companyLinkedin.formvalidClass.join(' ')} placeholder="Twitter link" title="" id="id_company-twitter"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-linkedin">LinkedIn link</label><input type="url" name="company-linkedin" value={this.state.form.companyLinkedin.value} onChange={(event)=>this.inputChangeHandler('companyLinkedin',event)}  className={this.state.form.companyTwitter.formvalidClass.join(' ')} placeholder="LinkedIn link" title="" id="id_company-linkedin"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-instagram">Instagram link</label><input type="url" name="company-instagram" value={this.state.form.companyInstagram.value} onChange={(event)=>this.inputChangeHandler('companyInstagram',event)} className={this.state.form.companyInstagram.formvalidClass.join(' ')} placeholder="Instagram link" title="" id="id_company-instagram"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-youtube">Youtube channel</label><input type="url" name="company-youtube" value={this.state.form.companyYoutube.value} onChange={(event)=>this.inputChangeHandler('companyYoutube',event)}  className={this.state.form.companyYoutube.formvalidClass.join(' ')} placeholder="Youtube channel" title="" id="id_company-youtube"/></div>
                                                    <div className="form-group col-12 col-sm-6"><label htmlFor="id_company-timezone">Time Zone</label>
                                                        <select name="company-timezone" className={this.state.form.companyTimezone.formvalidClass.join(' ')} title="" value={this.state.form.companyTimezone.value} required="" id="id_company-timezone"  aria-hidden="true" onChange={(event)=>this.inputChangeHandler('companyTimezone',event)}>
                                                            <option value="">---------</option>
                                                            {timezoneDropdown}
                                                        </select>

                                                    </div>
                                                    <div className="form-group col-12 col-sm-6">
                                                        <label htmlFor="id_company-status">Status</label>
                                                        <select name="company-status" className={this.state.form.companyStatus.formvalidClass.join(' ')} value={this.state.form.companyStatus.value}  id="id_company-status" aria-hidden="true" onChange={(event)=>this.inputChangeHandler('companyStatus',event)}>
                                                            <option value="Active">Active</option>
                                                            <option value="Inactive">Inactive</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                 <div className="row" data-form-list="address-prefix">
                                                        <div id="address-0-row" className="dynamic-form row mt-2 form-buttons-w">
                                                            <div className="col-12">
                                                                <a className="float-right add-row" onClick={this.showSection.bind(this)} data-prefix="address" title="Add New Address Type">
                                                                    <i className="os-icon os-icon-coins-4 mx-1 text-primary"></i>
                                                                </a>
                                                            </div>

                                                            <div className="form-group col-12 col-sm-6">
                                                                <label htmlFor="id_address-0-type">Address Type</label>
                                                                <select name="address-0-type" className={this.state.form.addressTypeOne.formvalidClass.join(' ')} value={this.state.form.addressTypeOne.value}  title="" id="id_address-0-type"  aria-hidden="true" onChange={(event)=>this.inputChangeHandler('addressTypeOne',event)}>
                                                                    <option value="">------</option>
                                                                    <option value="Billing">Billing</option>
                                                                    <option value="Building">Building</option>
                                                                    <option value="Residential">Residential</option>
                                                                    <option value="Office">Office</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-line_1">Address</label><input type="text" name="address-0-line_1" onChange={(event)=>this.inputChangeHandler('addressLineOneOne',event)} value={this.state.form.addressLineOneOne.value}   className={this.state.form.addressLineOneOne.formvalidClass.join(' ')} placeholder="Address" title="" id="id_address-0-line_1"/></div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-line_2">Ste or Apt</label><input type="text" name="address-0-line_2" onChange={(event)=>this.inputChangeHandler('addressLineTwoOne',event)} value={this.state.form.addressLineTwoOne.value}   className={this.state.form.addressLineTwoOne.formvalidClass.join(' ')} placeholder="Ste or Apt" title="" id="id_address-0-line_2"/></div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-city">City</label><input type="text" name="address-0-city"  onChange={(event)=>this.inputChangeHandler('addressCityOne',event)} value={this.state.form.addressCityOne.value} className={this.state.form.addressCityOne.formvalidClass.join(' ')} placeholder="City" title="" id="id_address-0-city"/></div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-state">State</label>
                                                                <select name="address-0-state" className={this.state.form.addressStateOne.formvalidClass.join(' ')} data-type="select2" title="" id="id_address-0-state"  aria-hidden="true" value={this.state.form.addressStateOne.value} onChange={(event)=>this.inputChangeHandler('addressStateOne',event)}>
                                                                <option value="">---------</option>
                                                                    {stateDropdown}
                                                                </select>
                                                            </div>
                                                            <div className="form-group col-12 col-sm-6">
                                                                <label htmlFor="id_address-0-zip">Zip</label>
                                                                <input type="text" onChange={(event)=>this.inputChangeHandler('addressZipOne',event)} name="address-0-zip"  value={this.state.form.addressZipOne.value} className={this.state.form.addressZipOne.formvalidClass.join(' ')} placeholder="Zip" title="" id="id_address-0-zip"/>
                                                            </div>
                                                            <div className="form-group col-12 col-sm-6">
                                                                <div className="form-check">
                                                                    <input type="checkbox" onChange={(event)=>this.inputChangeHandler('addressDefaultOne',event)} checked={this.state.form.addressDefaultOne.value} name="address-0-default" className="form-check-input"/>
                                                                    <label className="form-check-label" htmlFor="id_address-0-default">Default</label>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="address-0-id" id="id_address-0-id"/>

                                                        </div>

                                                     { this.state.addressTypeTwoShow ?
                                                        <div id="address-0-row" className="dynamic-form row mt-2 form-buttons-w">
                                                            <div className="col-12">
                                                                <span className="d-none">
                                                                    <input type="checkbox" name="address-0-DELETE" id="id_address-0-DELETE"/>
                                                                        <input type="hidden" name="address-0-id" id="id_address-0-id"/>
                                                                </span>
                                                                <a id="remove-address-1-row" onClick={this.hideSection.bind(this)} data-prefix="address" title="Remove Current Address Type" className="delete-row float-right mx-1">
                                                                    <i className="os-icon os-icon-cancel-circle text-danger"></i>
                                                                </a>
                                                            </div>

                                                            <div className="form-group col-12 col-sm-6">
                                                                <label htmlFor="id_address-0-type">Address Type</label>
                                                                <select name="address-0-type" className={this.state.form.addressTypeTwo.formvalidClass.join(' ')} value={this.state.form.addressTypeTwo.value} title="" id="id_address-0-type"  aria-hidden="true" onChange={(event)=>this.inputChangeHandler('addressTypeTwo',event)}>
                                                                    <option value="">------</option>
                                                                    <option value="Billing">Billing</option>
                                                                    <option value="Building">Building</option>
                                                                    <option value="Residential">Residential</option>
                                                                    <option value="Office">Office</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-line_1">Address</label><input type="text" name="address-0-line_1" value={this.state.form.addressLineOneTwo.value} onChange={(event)=>this.inputChangeHandler('addressLineOneTwo',event)}   className={this.state.form.addressLineOneTwo.formvalidClass.join(' ')} placeholder="Address" title="" id="id_address-0-line_1"/></div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-line_2">Ste or Apt</label><input type="text" name="address-0-line_2" value={this.state.form.addressLineTwoTwo.value} onChange={(event)=>this.inputChangeHandler('addressLineTwoTwo',event)}  className={this.state.form.addressLineTwoTwo.formvalidClass.join(' ')} placeholder="Ste or Apt" title="" id="id_address-0-line_2"/></div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-city">City</label><input type="text" name="address-0-city"  value={this.state.form.addressCityTwo.value} onChange={(event)=>this.inputChangeHandler('addressCityTwo',event)} className={this.state.form.addressCityTwo.formvalidClass.join(' ')} placeholder="City" title="" id="id_address-0-city"/></div>
                                                            <div className="form-group col-12 col-sm-6"><label htmlFor="id_address-0-state">State</label>
                                                                <select name="address-0-state" className={this.state.form.addressStateTwo.formvalidClass.join(' ')} data-type="select2" value={this.state.form.addressStateTwo.value} title="" id="id_address-0-state"  aria-hidden="true" onChange={(event)=>this.inputChangeHandler('addressStateTwo',event)}>
                                                                <option value="">---------</option>
                                                                    {stateDropdown}
                                                                </select>
                                                            </div>
                                                            <div className="form-group col-12 col-sm-6">
                                                                <label htmlFor="id_address-0-zip">Zip</label>
                                                                <input type="text" onChange={(event)=>this.inputChangeHandler('addressZipTwo',event)} name="address-0-zip"  value={this.state.form.addressZipTwo.value} className={this.state.form.addressZipTwo.formvalidClass.join(' ')} placeholder="Zip" title="" id="id_address-0-zip"/>
                                                            </div>
                                                            <div className="form-group col-12 col-sm-6">
                                                                <div className="form-check">
                                                                    <input type="checkbox" onChange={(event)=>this.inputChangeHandler('addressDefaultTwo',event)} defaultChecked={this.state.form.addressDefaultTwo.value} name="address-1-default" className="form-check-input"/>
                                                                    <label className="form-check-label" htmlFor="id_address-0-default">Default</label>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="address-0-id" id="id_address-0-id"/>

                                                        </div>:null}

                                                        <button className="btn btn-primary float-right" onClick={this.submitHandler.bind(this)}><i className="far fa-check-double"></i> Submit Form</button>
                                                 </div>
                                        </form>
                                    </div>



                                </div>
                            </div>

                        </div>
                    </div>
            </div>
        )
    }
}
export default CompanyCreateEdit;