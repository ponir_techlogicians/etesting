import React from 'react';
const companyEdit= (props)=>
    (
        <div class="content-box">
            <div class="element-wrapper">
                <div class="element-box col">
                    <h5 class="form-header"><i class="fal fa-layer-plus"></i> Edit Company
                        <small>
                            <a href="/company/list/" title="View Companies" data-toggle="tooltip" class="float-right"><i class="fal fa-list"></i> Companies</a>
                        </small>
                    </h5>
                    <div class="form-desc"></div>
                    <div class="row justify-content-md-center">
                        <div class="col-12 col-sm-11">

                            <div class="content-box">
                                <form method="POST" id="company-form" novalidate="">
                                        <input type="hidden" name="csrfmiddlewaretoken" value="wZ7ppXHE0XMnQviiuFCu2uwdkyMiHw96j0Ew5WKMIpL5FhAQuRBUcthghAnTtbpt" />
                                        <div class="row">
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-name">Name</label><input type="text" name="company-name" maxlength="254" class="form-control" placeholder="Name" title="" required="" id="id_company-name"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-phone">Phone</label><input type="text" name="company-phone" maxlength="15" class="form-control" placeholder="Phone" title="" required="" id="id_company-phone"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-fax">Fax</label><input type="text" name="company-fax" maxlength="15" class="form-control" placeholder="Fax" title="" id="id_company-fax"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-email">Email</label><input type="email" name="company-email" maxlength="128" class="form-control" placeholder="Email" title="" required="" id="id_company-email"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-website">Website</label><input type="url" name="company-website" maxlength="254" class="form-control" placeholder="Website" title="" id="id_company-website"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-facebook">Facebook link</label><input type="url" name="company-facebook" maxlength="254" class="form-control" placeholder="Facebook link" title="" id="id_company-facebook"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-twitter">Twitter link</label><input type="url" name="company-twitter" maxlength="254" class="form-control" placeholder="Twitter link" title="" id="id_company-twitter"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-linkedin">LinkedIn link</label><input type="url" name="company-linkedin" maxlength="254" class="form-control" placeholder="LinkedIn link" title="" id="id_company-linkedin"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-instagram">Instagram link</label><input type="url" name="company-instagram" maxlength="254" class="form-control" placeholder="Instagram link" title="" id="id_company-instagram"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-youtube">Youtube channel</label><input type="url" name="company-youtube" maxlength="254" class="form-control" placeholder="Youtube channel" title="" id="id_company-youtube"/></div>
                                            <div class="form-group col-12 col-sm-6"><label for="id_company-timezone">Time Zone</label>
                                                <select name="company-timezone" class="form-control select2 select2-hidden-accessible" title="" required="" id="id_company-timezone" tabindex="-1" aria-hidden="true">
                                                    <option value="" selected="">---------</option>

                                                    <option value="b7f4996c-17c2-4d43-ab51-c6de2ce60fd3">Pacific/Midway</option>

                                                    <option value="c13390d9-e3e1-484d-8c74-14ffe5d91b49">Pacific/Honolulu</option>

                                                    <option value="b3c84410-9c55-480b-9c22-e3685bbdac25">America/Los_Angeles</option>

                                                    <option value="437f0453-743c-4930-a348-13ad23e58b60">America/Phoenix</option>

                                                    <option value="b68a0859-914f-4df2-948a-8f03a68517fc">America/Los_Angeles</option>

                                                    <option value="6f935804-b0e0-482a-936e-b3c6acfa5318">America/Denver</option>

                                                    <option value="a4abfdc6-f9cd-4ed9-8f24-d36a1cf11f54">America/Chicago</option>

                                                    <option value="80de91c9-04de-4a23-85bd-57c6693ebf0e">America/New_York</option>

                                                    <option value="bb035ca1-a3a3-44a1-b391-2203a0495b9d">Atlantic/Bermuda</option>

                                                    <option value="799f569e-88a4-4c5e-b5d6-e2b17a41d37b">Atlantic/Cape_Verde</option>

                                                    <option value="7f999521-c921-481c-84b9-9a7eff07b3d6">Atlantic/Reykjavik</option>

                                                    <option value="b2864109-ce33-4f39-8197-7520e66d16b0">Europe/London</option>

                                                    <option value="d0a36537-9967-403c-a09b-ba1613229e83">Europe/London</option>

                                                    <option value="f323ba73-adb5-4e1c-85e1-ae049ae6a7f1">Africa/Lagos</option>

                                                    <option value="39744cb5-48fe-4159-80f5-5d1b5d56a746">Europe/Paris</option>

                                                    <option value="f151282b-64e1-498a-8c57-561622b3cbd6">Europe/Rome</option>

                                                    <option value="d700e576-5005-40ad-a166-154a3fe17c43">Asia/Jerusalem</option>
                                                </select>

                                            </div>
                                            <div class="form-group col-12 col-sm-6">
                                                <label for="id_company-status">Status</label>
                                                <select name="company-status" class="form-control select2 select2-hidden-accessible" title="" id="id_company-status" tabindex="-1" aria-hidden="true">
                                                    <option value="Active" selected="">Active</option>

                                                    <option value="Inactive">Inactive</option>

                                                </select>
                                            </div>
                                        </div>
                                         <input type="hidden" name="address-TOTAL_FORMS" value="1" id="id_address-TOTAL_FORMS"/><input type="hidden" name="address-INITIAL_FORMS" value="0" id="id_address-INITIAL_FORMS"/><input type="hidden" name="address-MIN_NUM_FORMS" value="0" id="id_address-MIN_NUM_FORMS" /><input type="hidden" name="address-MAX_NUM_FORMS" value="2" id="id_address-MAX_NUM_FORMS"/>

                                         <div class="row" data-form-list="address-prefix">
                                                <div id="address-0-row" class="dynamic-form row mt-2 form-buttons-w">
                                                    <div class="col-12">
                                                        <span class="d-none">
                                                            <input type="checkbox" name="address-0-DELETE" id="id_address-0-DELETE"/>
                                                                <input type="hidden" name="address-0-id" id="id_address-0-id"/>
                                                        </span>
                                                        <a id="remove-address-0-row" href="javascript:void(0)" data-prefix="address" title="Remove Current Address Type" class="delete-row float-right mx-1 d-none">
                                                            <i class="os-icon os-icon-cancel-circle text-danger"></i>
                                                        </a>

                                                        <a class="float-right add-row " data-prefix="address" title="Add New Address Type">
                                                            <i class="os-icon os-icon-coins-4 mx-1 text-primary"></i>
                                                        </a>
                                                    </div>

                                                    <div class="form-group col-12 col-sm-6"><label for="id_address-0-type">Address Type</label>
                                                        <select name="address-0-type" class="form-control select2 select2-hidden-accessible" data-type="select2" title="" id="id_address-0-type" tabindex="-1" aria-hidden="true">
                                                            <option value="" selected="">------</option>

                                                            <option value="Billing">Billing</option>

                                                            <option value="Building">Building</option>

                                                            <option value="Residential">Residential</option>

                                                            <option value="Office">Office</option>

                                                        </select>
                                                    </div>
                                                    <div class="form-group col-12 col-sm-6"><label for="id_address-0-line_1">Address</label><input type="text" name="address-0-line_1" maxlength="254" class="form-control" placeholder="Address" title="" id="id_address-0-line_1"/></div>
                                                    <div class="form-group col-12 col-sm-6"><label for="id_address-0-line_2">Ste or Apt</label><input type="text" name="address-0-line_2" maxlength="254" class="form-control" placeholder="Ste or Apt" title="" id="id_address-0-line_2"/></div>
                                                    <div class="form-group col-12 col-sm-6"><label for="id_address-0-city">City</label><input type="text" name="address-0-city" maxlength="32" class="form-control" placeholder="City" title="" id="id_address-0-city"/></div>
                                                    <div class="form-group col-12 col-sm-6"><label for="id_address-0-state">State</label><select name="address-0-state" class="form-control select2 select2-hidden-accessible" data-type="select2" title="" id="id_address-0-state" tabindex="-1" aria-hidden="true">
                                                        <option value="" selected="">---------</option>

                                                        <option value="al">Alabama</option>

                                                        <option value="ak">Alaska</option>

                                                        <option value="as">American Samoa</option>

                                                        <option value="az">Arizona</option>

                                                        <option value="ar">Arkansas</option>

                                                        <option value="ca">California</option>

                                                        <option value="co">Colorado</option>

                                                        <option value="ct">Connecticut</option>

                                                        <option value="de">Delaware</option>

                                                        <option value="dc">District Of Columbia</option>

                                                        <option value="fm">Federated States Of Micronesia</option>

                                                        <option value="fl">Florida</option>

                                                        <option value="ga">Georgia</option>

                                                        <option value="gu">Guam</option>

                                                        <option value="hi">Hawaii</option>

                                                        <option value="id">Idaho</option>

                                                        <option value="il">Illinois</option>

                                                        <option value="in">Indiana</option>

                                                        <option value="ia">Iowa</option>

                                                        <option value="ks">Kansas</option>

                                                        <option value="ky">Kentucky</option>

                                                        <option value="la">Louisiana</option>

                                                        <option value="me">Maine</option>

                                                        <option value="mh">Marshall Islands</option>

                                                        <option value="md">Maryland</option>

                                                        <option value="ma">Massachusetts</option>

                                                        <option value="mi">Michigan</option>

                                                        <option value="mn">Minnesota</option>

                                                        <option value="ms">Mississippi</option>

                                                        <option value="mo">Missouri</option>

                                                        <option value="mt">Montana</option>

                                                        <option value="ne">Nebraska</option>

                                                        <option value="nv">Nevada</option>

                                                        <option value="nh">New Hampshire</option>

                                                        <option value="nj">New Jersey</option>

                                                        <option value="nm">New Mexico</option>

                                                        <option value="ny">New York</option>

                                                        <option value="nc">North Carolina</option>

                                                        <option value="nd">North Dakota</option>

                                                        <option value="mp">Northern Mariana Islands</option>

                                                        <option value="oh">Ohio</option>

                                                        <option value="ok">Oklahoma</option>

                                                        <option value="or">Oregon</option>

                                                        <option value="pw">Palau</option>

                                                        <option value="pa">Pennsylvania</option>

                                                        <option value="pr">Puerto Rico</option>

                                                        <option value="ri">Rhode Island</option>

                                                        <option value="sc">South Carolina</option>

                                                        <option value="sd">South Dakota</option>

                                                        <option value="tn">Tennessee</option>

                                                        <option value="tx">Texas</option>

                                                        <option value="ut">Utah</option>

                                                        <option value="vt">Vermont</option>

                                                        <option value="vi">Virgin Islands</option>

                                                        <option value="va">Virginia</option>

                                                        <option value="wa">Washington</option>

                                                        <option value="wv">West Virginia</option>

                                                        <option value="wi">Wisconsin</option>

                                                        <option value="wy">Wyoming</option>

                                                    </select>
                                                    </div>
                                                    <div class="form-group col-12 col-sm-6"><label for="id_address-0-zip">Zip</label><input type="text" name="address-0-zip" maxlength="8" class="form-control" placeholder="Zip" title="" id="id_address-0-zip"/></div>
                                                    <div class="form-group col-12 col-sm-6">
                                                        <div class="form-check">
                                                            <input type="checkbox" name="address-0-default" class="form-check-input" id="id_address-0-default"/>
                                                            <label class="form-check-label" for="id_address-0-default">Default</label>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="address-0-id" id="id_address-0-id"/>

                                                </div>


                                                <button class="btn btn-primary float-right"><i class="far fa-check-double"></i> Submit Form</button>
                                         </div>
                                </form>
                            </div>



                        </div>
                    </div>

                </div>
            </div>
        </div>
    );

export default companyEdit;
