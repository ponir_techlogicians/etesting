import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import CompanyTableItem from './../../components/company-table-item/companytableitem';
class CompanyList extends Component{
    state={
        companies:[]
    }
    componentDidMount(){
       axios.get('http://127.0.0.1:8000/api/core/company/')
            .then(
                (res)=>{
                    console.log(res.data.results);
                    this.setState({companies:res.data.results});
                }
            )
    }
    render(){
        return (
                <div className="content-box">
                    <div className="element-wrapper">
                        <div className="element-box">
                            <h5 className="form-header">
                                <i className="fa fa-id-card-o"></i> Manage Companies
                                <small><Link to="/company/add" title="Add New Company" data-toggle='tooltip' className="float-right"><i className="fal fa-layer-plus"></i> Add Company</Link></small>
                            </h5>
                            <div className="table-responsive">
                                <table id="company-table" width="100%" className="table table-striped table-sm table-lightfont">
                                    <thead>
                                        <tr>
                                            <th className="text-center">*</th>
                                            <th>Status</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                           {this.state.companies.map(company=>
                                                <CompanyTableItem key={company.id} id={company.id} name={company.name} status={company.status} phone={company.phone}></CompanyTableItem>
                                            )}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
        );

    }
}
export default CompanyList;
