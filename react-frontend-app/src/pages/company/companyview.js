import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

class CompanyView extends Component{

    state={
        company:{
            name:'',
            addresses: [],
            created: "",
            email: "",
            enabled: true,
            facebook: "",
            fax: "",
            id: "",
            instagram: "",
            linkedin: "",
            phone: "",
            status: "",
            timezone: {
                "id": "",
                "utc": "",
                "label": "",
                "alias": "",
            },
            twitter: "",
            updated: "",
            updated_by: 1,
            website: "",
            youtube: "",
        }
    };

    componentDidMount(){
        const companyId = this.props.match.params.id;
        axios.get('http://127.0.0.1:8000/api/core/company/'+companyId+"/")
            .then((res)=>{
                console.log(res.data);
                this.setState({company:res.data});
            })
    }
    render(){
        return (
            <div class="content-box">
                <div class="element-wrapper">
                    <div class="element-box col">
                        <h5 class="form-header">
                            <i class="fa fa-id-card-o"></i> View Companies
                            <small>
                                <Link to="/company/list" title="View Companies" data-toggle='tooltip' class="float-right"><i class="fal fa-list"></i> Companies</Link>
                            </small>
                        </h5>
                        <div class="form-desc"></div>
                        <div class="row justify-content-md-center">
                            <div class="col-12 col-sm-11">
                                <div class="row">
                                    <div class="element-wrapper">
                                        <div class="element-box"><h6 class="element-header">{this.state.company.name}</h6>
                                            <div class="timed-activities compact">
                                                <div class="timed-activity">
                                                    <div class="ta-date"><span>{this.state.company.phone} / {this.state.company.email}</span></div>
                                                    <div class="ta-record-w">
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Website</strong></div>
                                                            <div class="ta-activity">{this.state.company.website}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Facebook</strong></div>
                                                            <div class="ta-activity">{this.state.company.facebook}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Twitter</strong></div>
                                                            <div class="ta-activity">{this.state.company.twitter}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Linkedin</strong></div>
                                                            <div class="ta-activity">{this.state.company.linkedin}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Instagram</strong></div>
                                                            <div class="ta-activity">{this.state.company.instagram}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Youtube</strong></div>
                                                            <div class="ta-activity">{this.state.company.youtube}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Timezone</strong></div>
                                                            <div class="ta-activity">{this.state.company.timezone.utc}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Status</strong></div>
                                                            <div class="ta-activity">{this.state.company.status}</div>
                                                        </div>
                                                        <div class="ta-record">
                                                            <div class="ta-timestamp"><strong>Website</strong></div>
                                                            <div class="ta-activity">{this.state.company.website}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="element-wrapper">
                                        <div class="element-box"><h6 class="element-header">{this.state.company.name} Addresses</h6>
                                            <div class="timed-activities compact">
                                                {this.state.company.addresses.map((item)=>
                                                    <div class="timed-activity">
                                                        <div class="ta-date"><span class="text-success">{item.line_1} ({item.type})</span></div>
                                                        <div class="ta-record-w">
                                                            <div class="ta-record">
                                                                <div class="ta-timestamp">
                                                                    <strong>{item.line_1} {item.line_2} <br/>{item.city} {item.zip} {item.state.name}<br/>{item.country.name}</strong>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 )}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CompanyView;
